using System;
using KreedzModule.Services;

namespace KurisuDiscordBot.Tests
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            var api = new GlobalAPI();
            var map = api.GetMap("kz_blackness");
            var recordTask = api.GetWorldRecord(map, "kz_timer");
            recordTask.Wait();
            var record = recordTask.Result;

            Console.WriteLine(record.MapName);
            Console.WriteLine(record.Mode);
            Console.WriteLine("Hey there");
        }
    }
}