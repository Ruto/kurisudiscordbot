using System.Threading.Tasks;
using KreedzModule.Services;
using Xunit;

namespace KurisuDiscordBot.Tests
{
    public class GlobalAPITests
    {
        private readonly GlobalAPI _api;

        public GlobalAPITests()
        {
            _api = new GlobalAPI();
        }

        [Fact]
        public async Task ShouldHaveMaps()
        {
            Assert.NotEmpty(await _api.GetMaps());
            Assert.NotEmpty(_api.Maps);
        }

        [Theory]
        [InlineData("kz_reach_v2", "kz_simple")]
        [InlineData("kz_dzy_reach_v2", "kz_timer")]
        [InlineData("kz_blackness", "kz_timer")]
        [InlineData("kz_dejavu", "kz_timer")]
        [InlineData("skz_odious", "kz_simple")]
        [InlineData("vnl_cat", "kz_vanilla")]
        public async Task ShouldHaveRecords(string map, string mode)
        {
            var apiMap = _api.GetMap(map);
            Assert.NotNull(apiMap);

            var record = await _api.GetWorldRecord(apiMap, mode);
            Assert.NotNull(record);
            Assert.Equal(record.Mode, mode, true);
            Assert.Equal(record.MapName, apiMap.Name, true);
        }
    }
}