using System.Threading.Tasks;
using RunescapeModule.Services;
using Xunit;

namespace KurisuDiscordBot.Tests
{
    public class RunescapeTests
    {
        private readonly RunescapeService _api;

        public RunescapeTests()
        {
            _api = new RunescapeService();
        }

        [Theory]
        [InlineData("ruto")]
        [InlineData("B0aty")]
        public async Task ShouldHaveStats(string username)
        {
            var stats = await _api.GetStats(username);
            Assert.NotNull(stats);
            Assert.True(string.Equals(username, stats.Username, System.StringComparison.CurrentCultureIgnoreCase));
            Assert.NotEmpty(stats.Skills);
        }
    }
}