using System;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using KreedzModule.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.Discord;

namespace KurisuDiscordBot
{
    internal class Program
    {
        public static void Main(string[] args)
            => new Program().MainAsync(args).GetAwaiter().GetResult();

        public static string LOG_FILE { get; private set; } = "logs/log-.txt";
        public static char COMMAND_PREFIX { get; private set; } = '!';
        private DiscordSocketClient _client;
        private CommandService _commands;
        private IServiceProvider _services;

        public async Task MainAsync(string[] args)
        {
            var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .Build();

            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(config)
                // .WriteTo.Discord(ulong.Parse(config["Serilog:Discord:WebhookId"]), config["Serilog:Discord:WebhookToken"])
                .CreateLogger();

            _services = new ServiceCollection()
                .AddSingleton<IConfiguration>(config)
                .AddSingleton<ILogger>(Log.Logger)
                .BuildServiceProvider();

            LOG_FILE = config["Discord:LogFile"];
            COMMAND_PREFIX = config["Discord:CommandPrefix"][0];

            _client = new DiscordSocketClient();
            _commands = new CommandService(new CommandServiceConfig
            {
                CaseSensitiveCommands = false
            });
            _client.Log += Discord_Log;
            _commands.Log += Discord_Log;

            await InitCommands();
            await _client.LoginAsync(TokenType.Bot, config["Discord:BotToken"]);
            await _client.StartAsync();
            await _client.SetGameAsync("Ruto");
            Log.Information("Init done");

            await Task.Delay(Timeout.Infinite);
        }

        public async Task InitCommands(bool loadExternalModules = true)
        {
            _client.MessageReceived += Discord_MessageReceived;
            await _commands.AddModulesAsync(Assembly.GetEntryAssembly(), _services);

            if (loadExternalModules)
            {
                var modulePaths = Directory.GetFiles(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "*Module.dll");
                foreach (var path in modulePaths)
                    await _commands.AddModulesAsync(Assembly.LoadFile(path), _services);
            }

            foreach (var moduleInfo in _commands.Modules)
            {
                Log.Information("Module loaded: {0}", moduleInfo.Name);
            }
        }

        private async Task Discord_MessageReceived(SocketMessage messageParam)
        {
            if (!(messageParam is SocketUserMessage msg)) return;

            if (msg.Author.Id == _client.CurrentUser.Id || msg.Author.IsBot) return;

            int argPos = 0;
            if (!msg.HasCharPrefix(COMMAND_PREFIX, ref argPos)) return;

            var context = new SocketCommandContext(_client, msg);
            var result = await _commands.ExecuteAsync(context, argPos, _services);
        }

        private Task Discord_Log(LogMessage message)
        {
            switch (message.Severity)
            {
                case LogSeverity.Critical:
                    Log.Fatal(message.Message);
                    break;
                case LogSeverity.Error:
                    Log.Error(message.Exception, message.Message);
                    break;
                case LogSeverity.Warning:
                    Log.Warning(message.Message);
                    break;
                case LogSeverity.Info:
                    Log.Information(message.Message);
                    break;
                case LogSeverity.Verbose:
                    Log.Verbose(message.Message);
                    break;
                case LogSeverity.Debug:
                    Log.Debug(message.Message);
                    break;
            }
            return Task.CompletedTask;
        }
    }
}
