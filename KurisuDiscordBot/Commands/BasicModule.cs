using System.Text;
using System.Threading.Tasks;
using Discord.Commands;
using Discord.Commands.Builders;
using Microsoft.Extensions.Configuration;

namespace KurisuDiscordBot.Commands
{
    public class BasicModule : ModuleBase<SocketCommandContext>
    {
        private readonly IConfiguration _config;
        private CommandService _commands;

        public BasicModule(IConfiguration config)
        {
            _config = config;
        }

        protected override void OnModuleBuilding(CommandService commandService, ModuleBuilder builder)
        {
            _commands = commandService;
        }

        [Command("test")]
        public async Task TestCommand()
        {
            await ReplyAsync("Test command - " + _config["Test:Message"]);
        }

        [Command("modules")]
        public async Task ModuleList()
        {
            var builder = new StringBuilder();
            builder.AppendLine("**Modules loaded**");
            int index = 0;
            foreach (var moduleInfo in _commands.Modules)
            {
                builder.AppendFormat("\t{0}. {1} - \n", ++index, moduleInfo.Name, moduleInfo.Summary ?? "N/A");
            }
            await ReplyAsync(builder.ToString());
        }
    }
}