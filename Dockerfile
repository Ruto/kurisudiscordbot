FROM mcr.microsoft.com/dotnet/core/sdk:3.0-alpine as build

WORKDIR /app

# Restore Nuget packages
COPY *.sln .
COPY KurisuDiscordBot/*.csproj ./KurisuDiscordBot/
COPY KreedzModule/*.csproj ./KreedzModule/
RUN [ "dotnet", "restore" ]

# Build prod
COPY KurisuDiscordBot/. ./KurisuDiscordBot/
COPY KreedzModule/. ./KreedzModule/
WORKDIR /app/KurisuDiscordBot
RUN [ "dotnet", "publish", "-c", "Release", "-o", "out" ]

# Run app
FROM mcr.microsoft.com/dotnet/core/runtime:3.0-alpine as runtime
WORKDIR /app
COPY --from=build /app/KurisuDiscordBot/out ./
ENTRYPOINT [ "dotnet", "KurisuDiscordBot.dll" ]
# Link /app/logs volume
# Docker compose for databases?