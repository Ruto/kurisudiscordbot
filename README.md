# KurisuDiscordBot

> Main Application running the Discord Bot

[![pipeline status](https://gitlab.com/Ruto/kurisudiscordbot/badges/master/pipeline.svg)](https://gitlab.com/Ruto/kurisudiscordbot/commits/master)

## Setup

1. Add Webhook Id and Token in KurisuDiscordBot/appsettings.json for Discord Logging (Serilog.Sinks.Discord)
   - Create the file if it doesn't exist, sample json in `KurisuDiscordBot/appsettings.sample.json`
2. Add Bot token to KurisuDiscordBot/appsettings.json
3. Modules
   - Add or remove modules by editing KurisuDiscordBot/KurisuDiscordBot.csproj or
   - `dotnet add reference <location of module csproj>`
   - `dotnet remove reference <location of module csproj>`
4. Run
   - Docker
     - `docker build -t kurisubot:latest .`
     - `docker run --rm -d --name kurisubot kurisubot:latest`
     - > Maybe link volume for logs folder `app/KurisuDiscordBot/logs` for persistence
   - Dotnet
     - `cd KurisuDiscordBot`
     - `dotnet publish -C Release -o out`
     - `cd out`
     - `dotnet KurisuDiscordBot.dll`

#### Libraries

- [Discord.Net](https://www.nuget.org/packages/Discord.Net/) ([docs](https://discord.foxbot.me/docs/index.html)) - Discord API for C#
- [Discord.Net.Commands](https://www.nuget.org/packages/Discord.Net.Commands/) ([docs](https://discord.foxbot.me/docs/guides/commands/intro.html)) - Command system from Discord.Net for moodularity
- [Serilog](https://serilog.net/) - Logging
  - Logs to [Console](https://www.nuget.org/packages/Serilog.Sinks.Console), [Discord](https://www.nuget.org/packages/Serilog.Sinks.Discord/), [File](https://www.nuget.org/packages/Serilog.Sinks.File)
  - [Serilog.Settings.Configuration](https://www.nuget.org/packages/Serilog.Settings.Configuration) - Configuration from Configuration object
  - [Serilog.Sinks.Async](https://www.nuget.org/packages/Serilog.Sinks.Async/) - Async logging
- [Microsoft.Extensions.Configuration](https://www.nuget.org/packages/Microsoft.Extensions.Configuration/) - Configuration object
  - [Microsoft.Extensions.Configuration.Json](https://www.nuget.org/packages/Microsoft.Extensions.Configuration.Json/) - Configuration from JSON files
- [Microsoft.Extensions.DependencyInjection](https://www.nuget.org/packages/Microsoft.Extensions.DependencyInjection/) - DependencyInjection containers for Modules of Discord.Net.Commands

## [Modules](./MODULES.md)
