> All Modules uses
* [Discord.Net.Commands](https://www.nuget.org/packages/Discord.Net.Commands/) - Modular commands for [Discord.Net](https://www.nuget.org/packages/Discord.Net/)
* [Serilog](https://serilog.net/) - Logging

# KreedzModule
> Module for endpoints of the Global-API

## Commands
`!wr <map>`

#### Libraries
* [RestSharp](https://www.nuget.org/packages/RestSharp/) - REST API client


# RunescapeModule
> Module for Old School Runescape Hiscores

## Commands
`!osrs`
`!osrs set <username>`

#### Libraries
* [MongoDB.Driver](http://mongodb.github.io/mongo-csharp-driver/) - Data Store for tracking hiscores across time span