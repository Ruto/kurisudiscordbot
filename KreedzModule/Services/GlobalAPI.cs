using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KreedzModule.Models;
using Newtonsoft.Json;
using RestSharp;
using Serilog;

namespace KreedzModule.Services
{
    public class GlobalAPI
    {
        private readonly IRestClient _client;
        private ILogger Log { get; set; }
        public readonly List<Map> Maps = new List<Map>();
        public readonly string[] Modes = { "kz_timer", "kz_simple", "kz_vanilla" };

        public GlobalAPI(ILogger logger = null)
        {
            Log = logger;
            _client = new RestClient("http://kztimerglobal.com/api/v1.0");
            Initialize().Wait();
        }

        private async Task Initialize()
        {
            var maps = await GetMaps().ConfigureAwait(false);
            Maps.Clear();
            Maps.AddRange(maps);
        }

        public Map GetMap(string term)
        {
            return Maps.First(m => m.Name.IndexOf(term, StringComparison.InvariantCultureIgnoreCase) > -1);
        }

        public async Task<IEnumerable<Map>> GetMaps()
        {
            var request = new RestRequest("maps");
            return await _client.GetAsync<List<Map>>(request).ConfigureAwait(false);
        }

        public async Task<Record> GetWorldRecord(Map map, string mode)
        {
            // http://kztimerglobal.com/api/v1.0/records/top?map_name=kz_reach_v2&tickrate=128&stage=0&modes_list_string=kz_simple&limit=1
            var request = new RestRequest("records/top")
                .AddQueryParameter("map_name", map.Name)
                .AddQueryParameter("modes_list_string", mode)
                .AddQueryParameter("tickrate", "128")
                .AddQueryParameter("stage", "0")
                .AddQueryParameter("limit", "1");
            var response = await _client.GetAsync<List<Record>>(request);
            return response?.Count == 0 ? null : response[0];
        }
    }
}
