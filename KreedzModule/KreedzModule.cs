using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.Commands.Builders;
using KreedzModule.Models;
using KreedzModule.Services;
using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json;
using Serilog;

namespace KreedzModule
{
    [Summary("Module for endpoints of the CSGO:KZ Global-API")]
    public class KreedzModule : ModuleBase<SocketCommandContext>
    {
        public ILogger Log { get; set; }
        public IConfiguration Configuration { get;set; }
        private static GlobalAPI _api;
        private static IMongoClient MongoClient { get; set; }
        private static IMongoDatabase Database { get; set; }

        protected override void OnModuleBuilding(CommandService commandService, ModuleBuilder builder)
        {
            _api = new GlobalAPI(Log);
            var mongoHost = Configuration["Modules:Kreedz:Mongo:DB_HOST"];
            var mongoDbName = Configuration["Modules:Kreedz:Mongo:DB_NAME"];
            Log.Information("Host: {host}, Name: {name}", mongoHost, mongoDbName);
            try 
            {
                MongoClient = new MongoClient($"mongodb://{mongoHost}/{mongoDbName}");
                Database = MongoClient.GetDatabase(mongoDbName);
            }
            catch(Exception ex)
            {
                Log.Error("Error: {error}", ex.ToString());
            }
            Log.Information("[KZ] Initialized");
        }

        protected override void BeforeExecute(CommandInfo command)
        {
            Log.Information("KreedzModule - {0}", command.Name);
        }

        [Command("kzget")]
        [Summary("Get SteamID64 for the user")]
        public async Task GetCurrentId()
        {
            try 
            {
                var userId = Context.User.Id;
                var filter = Builders<DiscordUser>.Filter.Eq(f => f.DiscordId, userId);
                var userCollection = Database.GetCollection<DiscordUser>("users");
                var dbUser = await userCollection.Find(filter).FirstOrDefaultAsync();

                if (dbUser == null)
                    await ReplyAsync("You do not have a SteamID64 set");
                else
                    await ReplyAsync($"Your SteamID64 is {dbUser.SteamId64}");
            }
            catch (Exception ex)
            {
                await ReplyAsync("Error occured");
            }
        }

        [Command("kzset")]
        [Summary("Set your SteamID64")]
        public async Task SetSteamId(long id)
        {
            try 
            {
                var userId = Context.User.Id;
                var userCollection = Database.GetCollection<DiscordUser>("users");
                var result = await userCollection.FindOneAndUpdateAsync(
                    filter: Builders<DiscordUser>.Filter.Eq(f => f.DiscordId, userId),
                    update: Builders<DiscordUser>.Update.Set(f => f.SteamId64, id),
                    options: new FindOneAndUpdateOptions<DiscordUser> { IsUpsert = true, ReturnDocument = ReturnDocument.After }
                );
                
                if (result == null)
                    await ReplyAsync("Error setting steamid64");
                else
                    await ReplyAsync("SteamID64 set");
            }
            catch (Exception ex)
            {
                Log.Error("Error {error}", ex);
                await ReplyAsync("Error occured");
            }
        }

        [Command("wr")]
        [Summary("Get the world record for the given map for all modes")]
        public async Task GetWorldRecord([Remainder] string map)
        {
            try 
            {
                Map apiMap = _api.GetMap(map);
                if (apiMap == null)
                {
                    await ReplyAsync("No such map found for " + map);
                    return;
                }

                var records = new List<Record>();
                foreach (var mode in _api.Modes)
                {
                    var record = await _api.GetWorldRecord(apiMap, mode);
                    if (record != null)
                        records.Add(record);
                }

                var embedBuilder = new EmbedBuilder()
                    .WithTitle($"World Record for {apiMap.Name}")
                    .WithColor(Color.Red)
                    .WithFooter("Ruto");

                foreach (var record in records)
                {
                    var time = record.Teleports == 0 ? "PRO" : $"{record.Teleports} TP";
                    embedBuilder.AddField(record.Mode, $"{record.Time} by **{record.PlayerName}** - **{time}**");
                }
                await ReplyAsync(embed: embedBuilder.Build());
            }
            catch (Exception ex)
            {
                await ReplyAsync("Error occured");
            }
        }
    }
}
