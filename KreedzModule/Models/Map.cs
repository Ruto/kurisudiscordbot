using System;

namespace KreedzModule.Models
{
    public class Map
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Filesize { get; set; }
        public bool Validated { get; set; }
        public int Difficulty { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
        public long ApprovedBySteamId64 { get; set; }
    }
}