using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace KreedzModule.Models
{
    public class DiscordUser
    {
        [BsonId]
        public ObjectId  Id { get; set; }
        [BsonElement("discord_id")]
        public ulong DiscordId { get; set; }
        [BsonElement("steam_id64")]
        public long SteamId64 { get; set; }
    }
}