using System;

namespace KreedzModule.Models
{
    public class Record
    {
        public int Id { get; set; }
        public long SteamId64 { get; set; }
        public string PlayerName { get; set; }
        public string SteamId { get; set; }
        public int ServerId { get; set; }
        public int MapId { get; set; }
        public int Stage { get; set; }
        public string Mode { get; set; }
        public int Tickrate { get; set; }
        public double Time { get; set; }
        public int Teleports { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
        public long UpdatedBy { get; set; }
        public int RecordFilterId { get; set; }
        public string ServerName { get; set; }
        public string MapName { get; set; }
        public int Points { get; set; }
        public int ReplayId { get; set; }
    }
}

/*
{

"id": 424421,
"steamid64": 76561198046784327,
"player_name": "Sachburger",
"steam_id": "STEAM_1:1:43259299",
"server_id": 390,
"map_id": 459,
"stage": 0,
"mode": "kz_simple",
"tickrate": 128,
"time": 75.633,
"teleports": 0,
"created_on": "2018-05-01T19:37:30",
"updated_on": "2018-05-01T19:37:30",
"updated_by": 0,
"record_filter_id": 0,
"server_name": "VoxClimbing",
"map_name": "kz_reach_v2",
"points": 1000,
"replay_id": 0

} */
