namespace RunescapeModule.Models
{
    public class Skill
    {
        public int Rank { get; set; }
        public int Level { get; set; }
        public long Experience { get; set; }

        public Skill(int rank, int level, long exp)
        {
            Rank = rank;
            Level = level;
            Experience = exp;
        }
    }
}