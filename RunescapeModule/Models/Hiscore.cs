using System;
using System.Collections.Generic;

namespace RunescapeModule.Models
{
    public class Hiscore
    {
        public string Username { get; set; }
        public Dictionary<SkillType, Skill> Skills = new Dictionary<SkillType, Skill>();
        public DateTime Created { get; set; }

        public Hiscore()
        {
            Created = DateTime.UtcNow;
        }
    }
}