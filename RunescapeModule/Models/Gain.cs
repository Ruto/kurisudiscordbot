using System;
using System.Collections.Generic;

namespace RunescapeModule.Models
{
    public class Gain
    {
        public string Username { get; set; }
        public TimeSpan Length { get; set; }
        public Dictionary<SkillType, long> SkillGains = new Dictionary<SkillType, long>();

        public static Gain Diff(Hiscore oldH, Hiscore newH, TimeSpan length)
        {
            var gain = new Gain
            {
                Username = newH.Username,
                Length = length
            };

            foreach (var skill in newH.Skills)
            {
                gain.SkillGains.Add(skill.Key, skill.Value.Experience - oldH.Skills[skill.Key].Experience);
            }

            return gain;
        }
    }
}