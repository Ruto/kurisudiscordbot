using System;

namespace RunescapeModule
{
    internal static class Extensions
    {
        public static long ToUnixTimeMilliseconds(this DateTime date)
        {
            return ((DateTimeOffset)date).ToUnixTimeMilliseconds();
        }

        private static readonly DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public static DateTime FromUnixTime(this long unixTime)
        {
            return epoch.AddMilliseconds(unixTime);
        }
    }
}