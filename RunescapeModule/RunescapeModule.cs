using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.Commands.Builders;
using Microsoft.Extensions.Configuration;
using RunescapeModule.Services;
using Serilog;

namespace RunescapeModule
{
    [Group("osrs")]
    [Summary("Module for Runescape Hiscores")]
    public class RunescapeModule : ModuleBase<SocketCommandContext>
    {
        public ILogger Log { get; set; }
        public IConfiguration Configuration { get; set; }
        private RunescapeService _api;

        protected override void OnModuleBuilding(CommandService commandService, ModuleBuilder builder)
        {
            _api = new RunescapeService();

            Log.Information("[OSRS] Initialized");
        }

        protected override void BeforeExecute(CommandInfo command)
        {
            Log.Information("RunescapeModule - {0}", command.Name);
        }

        [Command]
        [Alias("get")]
        [Summary("Get your current set OSRS username")]
        public async Task GetUsername()
        {
            var authorId = this.Context.User.Id;
            await ReplyAsync("Test");
        }

        [Command("set")]
        [Alias("s")]
        [Summary("Set your OSRS username")]
        public async Task SetUsername([Remainder] string username)
        {
            await ReplyAsync($"Setting username to {username}");
        }

        private const int MaxLength = 50;
        private readonly string Divider = new string('-', MaxLength);

        [Command("stats")]
        [Summary("Get stats given name")]
        public async Task GetStats([Remainder] string name)
        {
            var stats = await _api.GetStats(name);
            string title = $"{stats.Username} on {stats.Created.ToShortDateString()}";
            var builder = new StringBuilder();
            builder.AppendLine(Divider);
            builder.AppendLine(title.PadLeft(((MaxLength - title.Length) / 2) + title.Length).PadRight(MaxLength));
            builder.AppendFormat("|{0,-20}|{1,9}|{2,18}|\n", "Skill", "Level", "Experience");
            builder.AppendLine(Divider);
            foreach (var (skillType, skill) in stats.Skills)
            {
                builder.AppendFormat("|{0,-20}|{1,9}|{2,18:n0}|\n", skillType, skill.Level, skill.Experience);
            }
            builder.AppendLine(Divider);
            await ReplyAsync($"```{builder.ToString()}```");
        }
    }
}