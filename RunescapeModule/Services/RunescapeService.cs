using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using RunescapeModule.Models;
using Serilog;

namespace RunescapeModule.Services
{
    public class RunescapeService
    {
        private readonly HttpClient _http;

        public RunescapeService()
        {
            _http = new HttpClient();
        }

        public async Task<Hiscore> GetStats(string username)
        {
            var url = "https://secure.runescape.com/m=hiscore_oldschool/index_lite.ws?player=" + HttpUtility.UrlEncode(username, Encoding.UTF8);
            var response = await _http.GetStringAsync(url).ConfigureAwait(false);
            var lines = response.Split('\n');

            var hiscore = new Hiscore { Username = username };
            for (int i = 0; i < (int)SkillType.Count; i++)
            {
                var chunk = lines[i].Split(',');
                hiscore.Skills.Add((SkillType)i, new Skill(int.Parse(chunk[0]), int.Parse(chunk[1]), int.Parse(chunk[2])));
            }

            var doc = hiscore;
            //await HiscoreCollection.InsertOneAsync(doc);
            return hiscore;
        }

    }
}