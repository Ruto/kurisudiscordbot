using System;
using System.Threading.Tasks;
using RunescapeModule.Models;

namespace RunescapeModule.Services
{
    // public class RunescapeService
    // {
    //     private readonly HttpClient _http;
    //     private readonly ILogger _logger;
    //     private IMongoDatabase _db;

    //     private IMongoCollection<Hiscore> HiscoreCollection => _db.GetCollection<Hiscore>("hiscores");

    //     public RunescapeService(HttpClient http, ILogger logger, IMongoDatabase db)
    //     {
    //         _http = http;
    //         _logger = logger;
    //         _db = db;
    //     }

    //     public async Task<Hiscore> GetStats(string username)
    //     {
    //         var url = "https://secure.runescape.com/m=hiscore_oldschool/index_lite.ws?player=" + HttpUtility.UrlEncode(username, Encoding.UTF8);
    //         var response = await _http.GetStringAsync(url);
    //         var lines = response.Split('\n');

    //         var hiscore = new Hiscore { Username = username };
    //         for (int i = 0; i < (int)SkillType.Count; i++)
    //         {
    //             var chunk = lines[i].Split(',');
    //             hiscore.Skills.Add((SkillType)i, new Skill(int.Parse(chunk[0]), int.Parse(chunk[1]), int.Parse(chunk[2])));
    //         }

    //         var doc = hiscore;
    //         await HiscoreCollection.InsertOneAsync(doc);
    //         _logger.LogDebug("[RS] Retrieved hiscore for {name}", username);
    //         return hiscore;
    //     }

    //     public async Task<Gain> GetGains(string username, TimeSpan since)
    //     {
    //         var currentHiscore = await GetStats(username);
    //         var dateSince = DateTime.UtcNow.Subtract(since);

    //         var filterBuilder = Builders<Hiscore>.Filter;
    //         var filter = filterBuilder.Gte("created", dateSince) & filterBuilder.Eq("username", username);
    //         var projection = Builders<Hiscore>.Projection.Exclude("_id");

    //         var oldHiscore = BsonSerializer.Deserialize<Hiscore>(await HiscoreCollection.Find(filter).Project(projection).FirstAsync());

    //         return Gain.Diff(oldHiscore, currentHiscore, since);
    //     }
    // }

}